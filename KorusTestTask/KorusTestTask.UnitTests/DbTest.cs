﻿using KorusTestTask.WebService.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace KorusTestTask.UnitTests
{
    [TestClass]
    public class DbTest
    {
        [TestMethod]
        public void TestEmployeeQuery()
        {
            var employees = KorusContext.GetAllEmployees();
            Assert.AreEqual(10, employees.Count);
        }

        [TestMethod]
        public void TestGetPositionAverageSalary()
        {
            var positions = KorusContext.GetPositionAverageSalary();
            Assert.AreEqual(2, positions.Count);
        }

        [TestMethod]
        public void TestGetEmployeeSalaryGreaterPositionAverage()
        {
            int count = KorusContext.GetEmployeeSalaryGreaterPositionAverage();
            Assert.AreEqual(3, count);
        }

        [TestMethod]
        public void TestGetEmployeesBirthdayNextMonth()
        {
            string employees = KorusContext.GetEmployeesBirthdayNextMonth();
            Assert.AreEqual("Dmitry, Mikhail, Slava, Vladimir", employees);
        }

    }
}
