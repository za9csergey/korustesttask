﻿USE KorusDb

SELECT p.*, ASTable.Salary
FROM Employees, (SELECT count(*) as count, ep.PositionId, avg(e.Salary) as Salary
	  FROM EmployeesPositions as ep
	  INNER JOIN Employees as e ON ep.EmployeeId = e.EmployeeId
	  GROUP BY ep.PositionId) as ASTable
	Inner Join Positions as p ON p.PositionId = ASTable.PositionId
GROUP BY p.PositionId, p.Title, ASTable.Salary
HAVING ASTable.Salary > AVG(Employees.Salary)
ORDER BY ASTable.Salary
	
	
