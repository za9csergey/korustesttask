﻿USE KorusDb

SELECT COUNT(*)
FROM EmployeesPositions as ep1
	  INNER JOIN (SELECT count(*) as count, ep.PositionId, avg(e.Salary) as AVGSalary
	  FROM EmployeesPositions as ep
	  INNER JOIN Employees as e ON ep.EmployeeId = e.EmployeeId
	  GROUP BY ep.PositionId) as ASTable
	  ON ep1.PositionId = ASTable.PositionId
	  INNER JOIN Employees as e1 On e1.EmployeeId = ep1.EmployeeId
	  WHERE e1.Salary > ASTable.AVGSalary

	 --Inner Join Positions as p ON p.PositionId = ASTable.PositionId
	 
