﻿using System.Collections.Generic;

namespace KorusTestTask.WebService.Models
{
    public class EmployeesUpdate
    {
        public List<Employee> EmployeesToAdd { get; set; } = new List<Employee>();
        public List<Employee> EmployeesToUpdate { get; set; } = new List<Employee>();
    }
}