﻿using KorusTestTask.WebService.Services;
using System;
using System.Collections.Generic;

namespace KorusTestTask.WebService.Models
{
    /// <summary>
    /// Employee model for Employee table
    /// </summary>
    public class Employee
    {
        public int EmployeeId { get; set; }
        public string Name { get; set; }
        public DateTime Birthday { get; set; }
        public int Salary { get; set; }

        public Telephone EmployeeTelephone { get; set; }
        public List<Position> EmployeePositions { get; set; } = new List<Position>();

        public Employee() { }

        public Employee(int employeeId, string name, DateTime birthday, int salary)
        {
            EmployeeId = employeeId;
            Name = name;
            Birthday = birthday;
            Salary = salary;
        }

        protected bool Equals(Employee other)
        {
            return EmployeeId == other.EmployeeId && string.Equals(Name, other.Name) && 
                   Birthday.Equals(other.Birthday) && Salary == other.Salary && 
                   Equals(EmployeeTelephone, other.EmployeeTelephone) && 
                   Equals(EmployeePositions, other.EmployeePositions);
        }

        

        public bool DataEquals(Employee other)
        {
            return string.Equals(Name, other.Name, StringComparison.OrdinalIgnoreCase) &&
                   Birthday.Equals(other.Birthday) && Salary == other.Salary &&
                   EmployeeTelephone.DataEquals(other.EmployeeTelephone) &&
                   EmployeePositions.CheckPositinsList(other.EmployeePositions);
        }
    }
}