﻿using System.Collections.Generic;

namespace KorusTestTask.WebService.Models
{
    /// <summary>
    /// Position model for table Position 
    /// </summary>
    public class Position
    {
        public int PositionId { get; set; }
        public string Title { get; set; }

        public List<Employee> Employees { get; set; } = new List<Employee>();

        public int AverageSalary { get; set; } 

        public Position() { }

        public Position(int positionId, string title)
        {
            PositionId = positionId;
            Title = title;
        }

        protected bool Equals(Position other)
        {
            return PositionId == other.PositionId && string.Equals(Title, other.Title) && 
                   Equals(Employees, other.Employees);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((Position) obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                int hashCode = PositionId;
                hashCode = (hashCode * 397) ^ (Title != null ? Title.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (Employees != null ? Employees.GetHashCode() : 0);
                return hashCode;
            }
        }
    }
}