﻿namespace KorusTestTask.WebService.Models
{
    /// <summary>
    /// Telephone model for table Telephones
    /// </summary>
    public class Telephone
    {
        public int TelephoneId { get; set; }
        public string Number { get; set; }

        public Employee EmployeeEntity { get; set; }

        public Telephone() { }

        public Telephone(int telephoneId, string number)
        {
            TelephoneId = telephoneId;
            Number = number;
        }

        protected bool Equals(Telephone other)
        {
            return TelephoneId == other.TelephoneId && string.Equals(Number, other.Number) &&
                   Equals(EmployeeEntity, other.EmployeeEntity);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((Telephone) obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                int hashCode = TelephoneId;
                hashCode = (hashCode * 397) ^ (Number != null ? Number.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (EmployeeEntity != null ? EmployeeEntity.GetHashCode() : 0);
                return hashCode;
            }
        }

        public bool DataEquals(Telephone otherEmployeeTelephone)
        {
            return Number == otherEmployeeTelephone.Number;
        }
    }
}