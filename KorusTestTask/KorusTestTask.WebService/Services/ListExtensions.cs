﻿using KorusTestTask.WebService.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace KorusTestTask.WebService.Services
{
    public static class ListExtensions
    {
        public static bool CheckEmloyeeId(this IEnumerable<Employee> employees, int employeeId)
        {
            Employee employee = employees.FirstOrDefault(x => x.EmployeeId == employeeId);
            return employee != null;
        }

        public static bool CheckUpdate(this IEnumerable<Employee> employees, Employee employee)
        {
            try
            {
                Employee sameIdEmployee = employees.FirstOrDefault(x => x.EmployeeId == employee.EmployeeId);

                if (sameIdEmployee == null)
                {
                    return true;
                }

                return sameIdEmployee.DataEquals(employee);
            }
            catch (NullReferenceException)
            {
                return true;
            }
        }

        public static bool CheckPositinsList(this List<Position> positions, List<Position> otherPositions)
        {
            try
            {
                if (positions.Count != otherPositions.Count)
                {
                    return false;
                }

                List<Position> newPositions = positions.Where(p =>
                    otherPositions.FirstOrDefault(op => op.Title == p.Title) != null).ToList();
                return newPositions.Count != 0;
            }
            catch (NullReferenceException)
            {
                return false;
            }
        }
    }
}