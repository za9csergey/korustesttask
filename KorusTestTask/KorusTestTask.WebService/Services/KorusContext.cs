﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;

namespace KorusTestTask.WebService.Models
{
    /// <summary>
    /// Context for KorusDb
    /// </summary>
    public static class KorusContext
    {
        private static string connectionString =
            @"Data Source=(localdb)\MSSQLLocalDB;Initial Catalog=KorusDb;Integrated Security=True;Connect Timeout=30;Encrypt=False;TrustServerCertificate=True;ApplicationIntent=ReadWrite;MultiSubnetFailover=False";

        private static string sqlScriptsFolder = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "SQLScripts/");
        /// <summary>
        /// Get all employee and empoyee's telephones and positions
        /// </summary>
        /// <returns>List of employees</returns>
        public static List<Employee> GetAllEmployees()
        {
            List<Employee> employees = new List<Employee>();
            try
            {
                using (var connection = new SqlConnection(connectionString))
                {
                    connection.Open();
                    using (SqlCommand employeeCommand = connection.CreateCommand(),
                        positionCommand = connection.CreateCommand())
                    {
                        // set query to employees and telephones table
                        employeeCommand.CommandText =
                            "SELECT * FROM Employees e inner JOIN Telephones t ON e.EmployeeId = t.EmployeeId";

                        // set query to get list of positions for employee
                        positionCommand.CommandText =
                            "select p.* FROM EmployeesPositions ep inner join Positions p ON ep.PositionId = p.PositionId WHERE ep.EmployeeId = @Id";
                        positionCommand.Parameters.Add("@Id", SqlDbType.Int);

                        // execute query to employee table
                        using (SqlDataReader employeeSqlDataReader = employeeCommand.ExecuteReader())
                        {
                            while (employeeSqlDataReader.Read())
                            {
                                // create employee instance
                                Employee employee = new Employee(
                                    employeeSqlDataReader.GetInt32(0),
                                    employeeSqlDataReader.GetString(1),
                                    employeeSqlDataReader.GetDateTime(2),
                                    employeeSqlDataReader.GetInt32(3)
                                );

                                // init employee telephone
                                employee.EmployeeTelephone = new Telephone(
                                    employeeSqlDataReader.GetInt32(4),
                                    employeeSqlDataReader.GetString(5));

                                employees.Add(employee);
                            }
                        }

                        // init employee position
                        foreach (Employee employee in employees)
                        {
                            // specify employeeId for query to positions table
                            positionCommand.Parameters[0].Value = employee.EmployeeId;
                            using (SqlDataReader positionDataReader = positionCommand.ExecuteReader())
                            {
                                while (positionDataReader.Read())
                                {
                                    // set position to current employee 
                                    employee.EmployeePositions.Add(new Position(
                                        positionDataReader.GetInt32(0),
                                        positionDataReader.GetString(1)));
                                }
                            }
                        }
                    }
                }

            }
            catch (SqlException e)
            {
                return null;
            }

            return employees.Count == 0 ? null : employees;
        }

        /// <summary>
        /// Get all data from telephones table
        /// </summary>
        /// <returns>List of telephones in database</returns>
        public static List<Telephone> GetAllTelephones()
        {
            List<Telephone> telephones = new List<Telephone>();
            using (var connection = new SqlConnection(connectionString))
            {
                connection.Open();
                using (var cmd = connection.CreateCommand())
                {
                    cmd.CommandText = "SELECT * FROM Telephones";
                    using (var dataReader = cmd.ExecuteReader())
                    {
                        while (dataReader.Read())
                        {
                            telephones.Add(new Telephone(
                                dataReader.GetInt32(0),
                                dataReader.GetString(1)));
                        }
                    }
                }
            }

            return telephones;
        }


        /// <summary>
        /// Get value of position's avarage salary for each position
        /// </summary>
        /// <returns>List of positions with set avaerageSalary property  </returns>
        public static List<Position> GetPositionAverageSalary()
        {
            List<Position> positions = new List<Position>();
            try
            {
                using (var connection = new SqlConnection(connectionString))
                {
                    connection.Open();
                    using (var cmd = connection.CreateCommand())
                    {
                        // subquery to get positionId and average salary
                        //string subQuery = "(SELECT count(*) as count, ep.PositionId, avg(e.Salary) as Salary " +
                        //                  "FROM EmployeesPositions as ep " +
                        //                  "INNER JOIN Employees as e ON ep.EmployeeId = e.EmployeeId " +
                        //                  "GROUP BY ep.PositionId) as ASTable";
                        //cmd.CommandText = $"SELECT p.*, ASTable.Salary FROM Employees, {subQuery} " +
                        //                  "Inner Join Positions as p ON p.PositionId = ASTable.PositionId " +
                        //                  "GROUP BY p.PositionId, p.Title, ASTable.Salary " +
                        //                  "HAVING ASTable.Salary > AVG(Employees.Salary) " +
                        //                  "ORDER BY ASTable.Salary ";
                        string path = Path.Combine(sqlScriptsFolder, "PositionAvgSalary.sql");
                        cmd.CommandText = File.ReadAllText(path);

                        // read data
                        SqlDataReader dataReader = cmd.ExecuteReader();
                        while (dataReader.Read())
                        {
                            Position position = new Position(
                                dataReader.GetInt32(0),
                                dataReader.GetString(1))
                            {
                                AverageSalary = dataReader.GetInt32(2)
                            };
                            positions.Add(position);
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                return null;
            }

            return positions;
        }

        /// <summary>
        /// Get count of employees have salary greater than their position average slary
        /// </summary>
        /// <returns></returns>
        public static int GetEmployeeSalaryGreaterPositionAverage()
        {
            int result = 0;
            try
            {
                using (var connection = new SqlConnection(connectionString))
                {
                    connection.Open();
                    using (var cmd = connection.CreateCommand())
                    {
                        string path = Path.Combine(sqlScriptsFolder, "EmployeeSalaryGreaterPositionAverage.sql");
                        cmd.CommandText = File.ReadAllText(path);

                        result = (Int32) cmd.ExecuteScalar();
                    }
                }
            }
            catch (SqlException e)
            {
                return 0;
            }

            return result;
        }

        /// <summary>
        /// Get Position with maximum count with sub 30 age old employees
        /// </summary>
        /// <returns></returns>
        public static string GetPositionEmployeesSub30CountMax()
        {
            string result = String.Empty;
            try
            {
                using (var connection = new SqlConnection(connectionString))
                {
                    connection.Open();
                    using (var cmd = connection.CreateCommand())
                    {
                        string path = Path.Combine(sqlScriptsFolder, "PositionEmployeesSub30CountMax.sql");
                        cmd.CommandText = File.ReadAllText(path);

                        SqlDataReader dataReader = cmd.ExecuteReader();
                        dataReader.Read();

                        result = dataReader.GetString(1);
                    }
                }
            }
            catch (SqlException e)
            {
                return String.Empty;
            }

            return result;
        }

        /// <summary>
        /// Get emloyees which birthday in next month
        /// </summary>
        /// <returns></returns>
        public static string GetEmployeesBirthdayNextMonth()
        {
            string result = String.Empty;
            try
            {
                using (var connection = new SqlConnection(connectionString))
                {
                    connection.Open();
                    using (var cmd = connection.CreateCommand())
                    {
                        string path = Path.Combine(sqlScriptsFolder, "EmployeesBirthdayNextMonth.sql");
                        cmd.CommandText = File.ReadAllText(path);

                        SqlDataReader dataReader = cmd.ExecuteReader();
                        List<string> employees = new List<string>();
                        while (dataReader.Read())
                        {
                            employees.Add(dataReader.GetString(0));
                        }

                        result = String.Join(", ", employees);
                    }
                }
            }
            catch (SqlException e)
            {
                return String.Empty;
            }


            return result;
        }
    }
}