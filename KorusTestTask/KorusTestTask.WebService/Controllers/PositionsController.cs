﻿using KorusTestTask.WebService.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;

namespace KorusTestTask.WebService.Controllers
{
    public class PositionsController : ApiController
    {
        // POST: api/Positions
        public IHttpActionResult Post([FromBody]string positionInfo)
        {
            string result = String.Empty;
            switch (positionInfo)
            {
                case "PositionSalaryGreaterAverage":
                    List<Position> positions = KorusContext.GetPositionAverageSalary();
                    IEnumerable<string> resultPositions = positions.Select(p => p.Title);
                    result = String.Join(", ", resultPositions);
                    break;
                case "EmployeeSalaryGreaterPositionAverage":
                    result = KorusContext.GetEmployeeSalaryGreaterPositionAverage().ToString();
                    break;

            }
            return Ok(result);
        }

        // PUT: api/Positions/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/Positions/5
        public void Delete(int id)
        {
        }
    }
}
