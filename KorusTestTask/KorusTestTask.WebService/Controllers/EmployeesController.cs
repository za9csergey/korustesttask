﻿using KorusTestTask.WebService.Models;
using KorusTestTask.WebService.Services;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;

namespace KorusTestTask.WebService.Controllers
{
    public class EmployeesController : ApiController
    {
        // GET: api/Employees
        public IEnumerable<Employee> Get()
        {
            var employees = KorusContext.GetAllEmployees();
            return employees;
        }

        // GET: api/Employees/5
        public string Get(int id)
        {
            return "value";
        }

        // POST: api/Employees
        public IHttpActionResult Post(IEnumerable<Employee> clientEmployees)
        {
            List<Employee> dbEmployees = KorusContext.GetAllEmployees();
            List<Employee> employeesToAdd = dbEmployees.Where(e => clientEmployees.CheckEmloyeeId(e.EmployeeId) == false).ToList();
            List<Employee> employeesToUpdate = dbEmployees.Where(e => clientEmployees.CheckUpdate(e) == false).ToList();
            List<Employee>[] updateData =
            {
                employeesToAdd, employeesToUpdate
            };
            string response = JsonConvert.SerializeObject(new
            {
                EmployeesToAdd = employeesToAdd,
                EmployeesToUpdate = employeesToUpdate
            });
            return Ok(response);
        }

        // PUT: api/Employees/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/Employees/5
        public void Delete(int id)
        {
        }
    }
}
