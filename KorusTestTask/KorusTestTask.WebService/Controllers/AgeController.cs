﻿using KorusTestTask.WebService.Models;
using System;
using System.Web.Http;

namespace KorusTestTask.WebService.Controllers
{
    public class AgeController : ApiController
    {
        // POST: api/Age
        public IHttpActionResult Post([FromBody]string value)
        {
            string result = String.Empty;
            switch (value)
            {
                case "PositionEmployeesSub30CountMax":
                    result = KorusContext.GetPositionEmployeesSub30CountMax();
                    break;
                case "EmployeesBirthdayNextMonth":
                    result = KorusContext.GetEmployeesBirthdayNextMonth();
                    break;
            }

            return Ok(result);
        }

        // PUT: api/Age/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/Age/5
        public void Delete(int id)
        {
        }
    }
}
