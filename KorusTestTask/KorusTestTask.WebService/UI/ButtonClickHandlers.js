﻿$(document).ready(function () {

    $("#PositionEmployeesSub30CountMaxButton").click(function() {
        var dataToPass = "PositionEmployeesSub30CountMax";
        $.post("api/age", { "": dataToPass },
            function (data) {
                $("#PositionEmployeesSub30CountMaxSpan").html(data);
            });
        return false;
    });

    $("#PositionSalaryGreaterAverageButton").click(function () {
        var dataToPass = "PositionSalaryGreaterAverage";
        $.post("api/positions", { "": dataToPass},
            function(data) {
                $("#PositionSalaryGreaterAverageSpan").html(data);
            });
        return false;
    });

    $("#EmployeeSalaryGreaterPositionAverageButton").click(function() {
        var dataToPass = "EmployeeSalaryGreaterPositionAverage";
        $.post("api/positions",
            { "": dataToPass },
            function(data) {
                $("#EmployeeSalaryGreaterPositionAverageSpan").html(data);
            });
        return false;
    });

    $("#EmployeesBirthdayNextMonthButton").click(function () {
        var dataToPass = "EmployeesBirthdayNextMonth";
        $.post("api/age", { "": dataToPass },
            function (data) {
                $("#EmployeesBirthdayNextMonthSpan").html(data);
            });
        return false;
    });

});