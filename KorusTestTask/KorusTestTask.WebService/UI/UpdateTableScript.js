﻿var storedData;

var itemTable;
// load data when page is ready
$(document).ready(function () {
    // send GET request
    $.getJSON("api/employees/",
        function (Data) {
            // initiate data on cliet side
            storedData = [];
            // get table element
            itemTable = $("#employeesTable tbody");
            // procces data from service
            $.each(Data,
                function(key, item) {
                    addNewRow(item);
                });
            console.log(storedData);
            
        })
        // when process is done start timer to update table every 20 seconds
        .done(window.setInterval(updateEmployees, 20000));
});

// save data on client side
function saveData(item, index) {
    console.log(index);
    // initialize positins array
    var positions = [];
    $.each(item.EmployeePositions, function(key, value) {
        positions.push({
            PositionId: value.PositionId,
            Title: value.Title
    });
    });

    var obj = {
        EmployeeId: item.EmployeeId,
        Name: item.Name,
        Birthday: item.Birthday,
        Salary: item.Salary,
        EmployeeTelephone: {
            TelephoneId: item.EmployeeTelephone.TelephoneId,
            Number: item.EmployeeTelephone.Number
        },
        EmployeePositions: positions
    }

    // update object in storedData array
    if (typeof index !== "undefined") {
        storedData[index] = obj;
        return false;

    }
    // save new data
    storedData.push(obj);
}

// update employees table
function updateEmployees() {
    // set POST request with storedData
    $.post("api/employees/",
        { '': storedData },
        function (data) {
            // parse response to JSON
            var jsonData = JSON.parse(data);
            // update existing rows
            updateRows(jsonData.EmployeesToUpdate);
            // add new rows 
            addNewRows(jsonData.EmployeesToAdd);
        });

}

// update existing rows
function updateRows(employees) {
    // get list of rows - tr tags
    var tableRows = $(".dataRow");
    // process list of employees to update
    $.each(employees,
        function (key, employee) {
            var employeeIndex;
            // iterate storedData to find index of record to update
            $.each(storedData,
                function (i, clientEmployee) {
                    // initialize employee index to update
                    if (clientEmployee.EmployeeId === employee.EmployeeId) {
                        employeeIndex = i;
                        return false;
                    }
                    return true;
                });
            // set new data to row with index
            tableRows.eq(employeeIndex).html(buildCells(employee));

            saveData(employee, employeeIndex);

        });
}

// add new rows 
function addNewRows(employees) {
    $.each(employees,
        function(key, employee) {
            addNewRow(employee);
        });
}

// add to table new row
function addNewRow(employee) {
    // build cells
    var cells = buildCells(employee);
    // build new row
    var tr = "<tr class=\"dataRow\">" + cells + "</tr>";
    // append to table and save data on client side
    itemTable.append(tr);
    saveData(employee);
}

// builds cells
function buildCells(item) {
    var name = "<td>" + item.Name + "</td>";
    // build birtday row
    var bdDate = new Date(item.Birthday);
    var birthday = "<td>" + bdDate.getFullYear() + "-" + bdDate.getMonth() + "-" + bdDate.getDate() + "</td>";
    var salary = "<td>" + item.Salary + "</td>";
    
    // build telephone number cell
    var number = "<td>" + buildTelephoneNumber(item.EmployeeTelephone.Number) + "</td>";
    // print positions
    var positonsCell = '<td>';
    $.each(item.EmployeePositions,
        function (positionKey, position) {
            positonsCell += position.Title + "&nbsp;";
        });
    positonsCell += "</td>";
    // append all in one row
    var cells = name + birthday + salary + number + positonsCell;
    return cells;
}

// build telephone number
function buildTelephoneNumber(number) {
    // use RegEx to get parts of number
    var parts = number.match("(^.)(.{2,3})(.{2,3})(.{2,2})(.{2,2})");
    // build string for phone
    var result = "+" + parts[1] + "(" + parts[2] + ")" + parts[3] + "-" + parts[4] + "-" + parts[5];
    return result;
}