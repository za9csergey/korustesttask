﻿USE KorusDb

INSERT INTO Employees(EmployeeId, Name, Birthday, Salary) VALUES(11, 'Ruslan', '1990-10-30', 50000);
INSERT INTO Telephones(TelephoneId, Number, EmployeeId) VALUES(11, '79315527879', 11);
INSERT INTO Employees(EmployeeId, Name, Birthday, Salary) VALUES(12, 'Kirill', '1991-10-30', 40000);
INSERT INTO Telephones(TelephoneId, Number, EmployeeId) VALUES(12, '79415527879', 12);

INSERT INTO EmployeesPositions(EmployeeId, PositionId) VALUES(11, 5);
INSERT INTO EmployeesPositions(EmployeeId, PositionId) VALUES(12, 5);

UPDATE Employees SET Name = 'Abdu', Birthday = '1991-08-30' WHERE EmployeeId = 4
UPDATE Employees SET Name = 'Ahmed', Birthday = '1991-05-05' WHERE EmployeeId = 5
UPDATE Telephones SET Number = '79215523388' WHERE EmployeeId = 3
UPDATE Telephones SET Number = '79218862245' WHERE EmployeeId = 2