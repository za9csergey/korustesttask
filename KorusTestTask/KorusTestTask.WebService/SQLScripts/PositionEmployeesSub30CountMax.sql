﻿USE KorusDb

SELECT TOP 1 p.*, ASTable.count
FROM Employees, (SELECT count(*) as count, ep.PositionId
	  FROM EmployeesPositions as ep
	  INNER JOIN Employees as e ON (ep.EmployeeId = e.EmployeeId AND DATEDIFF(hour,e.Birthday,GETDATE())/8766 < 30)	  
	  GROUP BY ep.PositionId
	  ) as ASTable
	Inner Join Positions as p ON p.PositionId = ASTable.PositionId	
	GROUP BY p.PositionId, p.Title, ASTable.count
	ORDER BY ASTable.count DESC
