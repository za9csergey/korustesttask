﻿
CREATE DATABASE KorusDb;
GO

USE KorusDb

CREATE TABLE Employees
(
	EmployeeId int NOT NULL PRIMARY KEY,
	Name varchar(255) NOT NULL,
	Birthday datetime NOT NULL,
	Salary int
);

CREATE TABLE Telephones
(
	TelephoneId int NOT NULL PRIMARY KEY,
	Number varchar(255) NOT NULL,
	EmployeeId int FOREIGN KEY REFERENCES Employees(EmployeeId)
);

CREATE TABLE Positions
(
	PositionId int NOT NULL PRIMARY KEY,
	Title varchar(255) NOT NULL,	
);

CREATE TABLE EmployeesPositions
(
	EmployeeId int FOREIGN KEY REFERENCES Employees(EmployeeId),
	PositionId int FOREIGN KEY REFERENCES Positions(PositionId)	
);
