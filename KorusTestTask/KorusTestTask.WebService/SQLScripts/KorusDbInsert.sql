﻿USE KorusDb

ALTER TABLE Telephones nocheck constraint all
ALTER TABLE EmployeesPositions nocheck constraint all
DELETE FROM EmployeesPositions;
DELETE FROM Employees;
DELETE FROM Telephones;
DELETE FROM Positions;
ALTER TABLE EmployeesPositions check constraint all
ALTER TABLE Telephones check constraint all


INSERT INTO Employees(EmployeeId, Name, Birthday, Salary) VALUES(1, 'Andrey', '1980-08-30', 40000);
INSERT INTO Employees(EmployeeId, Name, Birthday, Salary) VALUES(2, 'Dmitry', '1985-05-20', 190000);
INSERT INTO Employees(EmployeeId, Name, Birthday, Salary) VALUES(3, 'Alex', '1990-04-30', 190000);
INSERT INTO Employees(EmployeeId, Name, Birthday, Salary) VALUES(4, 'Mikhail', '1988-05-12', 80000);
INSERT INTO Employees(EmployeeId, Name, Birthday, Salary) VALUES(5, 'Pavel', '1987-02-02', 60000);
INSERT INTO Employees(EmployeeId, Name, Birthday, Salary) VALUES(6, 'Danila', '1991-09-23', 50000);
INSERT INTO Employees(EmployeeId, Name, Birthday, Salary) VALUES(7, 'Slava', '1987-05-03', 80000);
INSERT INTO Employees(EmployeeId, Name, Birthday, Salary) VALUES(8, 'Vladimir', '1991-05-07', 40000);
INSERT INTO Employees(EmployeeId, Name, Birthday, Salary) VALUES(9, 'Alexander', '1992-04-10', 100000);
INSERT INTO Employees(EmployeeId, Name, Birthday, Salary) VALUES(10, 'Petr', '1983-10-05', 80000);

INSERT INTO Telephones(TelephoneId, Number, EmployeeId) VALUES(1, '79215523364', 1);
INSERT INTO Telephones(TelephoneId, Number, EmployeeId) VALUES(2, '79219562245', 2);
INSERT INTO Telephones(TelephoneId, Number, EmployeeId) VALUES(3, '79450871234', 3);
INSERT INTO Telephones(TelephoneId, Number, EmployeeId) VALUES(4, '79672340656', 4);
INSERT INTO Telephones(TelephoneId, Number, EmployeeId) VALUES(5, '79217529123', 5);
INSERT INTO Telephones(TelephoneId, Number, EmployeeId) VALUES(6, '79216752892', 6);
INSERT INTO Telephones(TelephoneId, Number, EmployeeId) VALUES(7, '79219112245', 7);
INSERT INTO Telephones(TelephoneId, Number, EmployeeId) VALUES(8, '79210453434', 8);
INSERT INTO Telephones(TelephoneId, Number, EmployeeId) VALUES(9, '79211284556', 9);
INSERT INTO Telephones(TelephoneId, Number, EmployeeId) VALUES(10, '79215527879', 10);

INSERT INTO Positions(PositionId, Title) VALUES(1, 'Programm manager');
INSERT INTO Positions(PositionId, Title) VALUES(2, 'Lead');
INSERT INTO Positions(PositionId, Title) VALUES(3, 'Senior');
INSERT INTO Positions(PositionId, Title) VALUES(4, 'Middle');
INSERT INTO Positions(PositionId, Title) VALUES(5, 'Junior');
INSERT INTO Positions(PositionId, Title) VALUES(6, 'Tester');

INSERT INTO EmployeesPositions(EmployeeId, PositionId) VALUES(1, 6);
INSERT INTO EmployeesPositions(EmployeeId, PositionId) VALUES(1, 5);
INSERT INTO EmployeesPositions(EmployeeId, PositionId) VALUES(2, 2);
INSERT INTO EmployeesPositions(EmployeeId, PositionId) VALUES(2, 1);
INSERT INTO EmployeesPositions(EmployeeId, PositionId) VALUES(3, 2);
INSERT INTO EmployeesPositions(EmployeeId, PositionId) VALUES(3, 1);
INSERT INTO EmployeesPositions(EmployeeId, PositionId) VALUES(4, 3);
INSERT INTO EmployeesPositions(EmployeeId, PositionId) VALUES(5, 4);
INSERT INTO EmployeesPositions(EmployeeId, PositionId) VALUES(5, 6);
INSERT INTO EmployeesPositions(EmployeeId, PositionId) VALUES(6, 4);
INSERT INTO EmployeesPositions(EmployeeId, PositionId) VALUES(7, 3);
INSERT INTO EmployeesPositions(EmployeeId, PositionId) VALUES(8, 5);
INSERT INTO EmployeesPositions(EmployeeId, PositionId) VALUES(8, 6);
INSERT INTO EmployeesPositions(EmployeeId, PositionId) VALUES(9, 3);
INSERT INTO EmployeesPositions(EmployeeId, PositionId) VALUES(10, 3);